#  --------------------------------------------------------------------------  #
#                            Unicon Install Helper                             #
#                                                                              #
#  This makefile is intended to compile and install unicon on Linux 64bit      #
#  debain based distros. It has been tested on Ubuntu 14.04.1 and Mint 17.2.   #
#                                                                              #
#                                                                              #
#  --------------------------------------------------------------------------  #
#                                                                              #
#                                  Directions                                  #
#                                                                              #
#  1. Place this makefile in the same directory as the unicon source           #
#  distribution. (http://unicon.org/dist/uni.zip)                              #
#                                                                              #
#  2. Open a terminal and navigate to the directory containing the makefile    #
#  and uni.zip                                                                 #
#                                                                              #
#  3. In the terminal run the command sudo make                                #
#                                                                              #
#  4. You will be requested to enter your password                             #
#                                                                              #
#  5. If installed successfully you should see a Install_Report.txt            #
#                                                                              #
#  6. A manual command will be required to complete instillation SEE           #
#  Install_Report.txt for command                                              #
#                                                                              #
#                                    Unicon                                    #
#                              http://unicon.org/                              #
#                                                                              #
#  --------------------------------------------------------------------------  #
#  Makefile Author: Arthur Putnam                                              #
#  visit: www.binary-thoughts.com                                              #
#                                                                              #
#  This program is free to USE at your own risk. You can redistribute it       #
#  and/or modify it under the terms and conditions of the AP-License           #
#  Agreement. This program is distributed WITHOUT ANY WARRANTY; without even   #
#  the implied warranty of purpose or USE. USE at your own RISK. For full      #
#  license details view AP-License Agreement. See                              #
#  http://www.binary-thoughts.net/ap-license.php                               #
#                                                                              #
#  --------------------------------------------------------------------------  #



OS_NAME := x86_64_linux
PACKAGE := uni.zip
DESTINATION := /opt/unicon/
STEPS := dependencies clean build move results





default: all

all: $(STEPS)

dependencies: .dependencies
.dependencies:
	apt-get -y update
	apt-get -y install build-essential
	apt-get -y install libx11-dev

clean: .clean
.clean:
	find . -name unicon | xargs rm -rf
	find . -name Install_Report.txt | xargs rm -f
	

build: .build
.build:
	unzip $(PACKAGE) 
	echo "Unzipped"
	
	
	cd unicon; \
	 make Configure name=$(OS_NAME) 
	 echo "Configure Complete"
	cd unicon; \
	 make Unicon
	 echo "Build Complete, Moving to $(DESTINATION)"

	
	

move: .move
.move:
	pwd
	mkdir /opt/unicon
	cp -r unicon/ $(DESTINATION)
	
	
results: .results
.results:
	echo "  -------------------------------------------------------------------  " >> Install_Report.txt
	echo "                         Unicon Install Report                        " >> Install_Report.txt
	echo "                                                                      " >> Install_Report.txt
	echo "  TO FINISH RUN: PATH=$$PATH:$(DESTINATION)unicon/bin                 " >> Install_Report.txt
	echo "                                                                      " >> Install_Report.txt
	echo "  ------------------------------------------------------------------- " >> Install_Report.txt
	echo "                                                                      " >> Install_Report.txt
	echo "  If you are seeing this file the makefile did not detect any         " >> Install_Report.txt
	echo "  errors. Hopefully this makefile helped you install unicon on a      " >> Install_Report.txt
	echo "  Linux based OS. After adding the “$(DESTINATION)unicon/bin” your    " >> Install_Report.txt
	echo "  path you should be able to check if unicon full installed by        " >> Install_Report.txt
	echo "  typing unicon in the terminal.                                      " >> Install_Report.txt
	echo "                                                                      " >> Install_Report.txt
	echo "                                Unicon                                " >> Install_Report.txt
	echo "                          http://unicon.org/                          " >> Install_Report.txt
	echo "                                                                      " >> Install_Report.txt
	echo "  Makefile Author: Arthur Putnam                                      " >> Install_Report.txt
	echo "  visit: www.binary-thoughts.com                                      " >> Install_Report.txt
	echo "                                                                      " >> Install_Report.txt
	echo "  -------------------------------------------------------------------  " >> Install_Report.txt
	clear
	@echo "IMPORTANT: Manual Command required. See Install_Report.txt."


